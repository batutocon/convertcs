using ICSharpCode.CodeConverter.Util;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Code Converter API", Version = "v1" });
});

builder.Services.AddEndpointsApiExplorer();

string localOrigins = "local";
builder.Services.AddCors(options => {
    options.AddPolicy(name: localOrigins, b =>
        b.WithOrigins("https://localhost:44433").AllowAnyMethod().AllowAnyHeader()
    );
    options.AddPolicy(name: localOrigins, b =>
        b.WithOrigins("https://localhost:7159/").AllowAnyMethod().AllowAnyHeader()
    );
});

var app = builder.Build();

if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

if (!app.Environment.IsDevelopment()) {
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseRouting();
app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(origin => true) // allow any origin
    .AllowCredentials()); // allow credentials

app.UseHttpsRedirection();

app.MapControllers();

app.Run();

