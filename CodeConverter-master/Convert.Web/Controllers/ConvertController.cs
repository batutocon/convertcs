﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Convert.Web.Controllers;
[ApiController]
[Route("api/[controller]")]
public class ConvertController : Controller
{
    private readonly ILogger<ConvertController> _logger;
    public ConvertController(ILogger<ConvertController> logger)
    {
        _logger = logger;
    }

    [HttpPost("/ConvertVB2CS")]
    public async Task<IActionResult> ConvertVB2CSAsync(string code)
    {
        ConvertRequest todo = new ConvertRequest(code, "vbnet2cs");
        ConvertResponse rs = await WebConverter.ConvertAsync(todo);
        return Ok(rs);
    }

    [HttpPost("/ConvertCS2VB")]
    public async Task<IActionResult> ConvertCS2VBAsync(string code)
    {
        ConvertRequest todo = new ConvertRequest(code, "cs2vbnet");
        ConvertResponse rs = await WebConverter.ConvertAsync(todo);
        return Ok(rs);
    }

    [HttpPost("/Convert")]
    public async Task<IActionResult> ConvertAsync(ConvertRequest todo)
    {
        ConvertResponse rs = await WebConverter.ConvertAsync(todo);
        return Ok(rs);
    }
}